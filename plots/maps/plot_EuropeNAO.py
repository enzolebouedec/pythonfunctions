from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt


def plot_EuropeNAO_Lambert(lons, lats, dx:float=2e4, dy:float=2e4,  ax=None):
    # If ax is not specified, create a new figure
    if ax is None:
            fig, ax = plt.subplots()
    
    # Compute center of the map
    lon_0 = lons.mean()
    lat_0 = lats.mean()

    # Compute the number of latitures and longitudes 
    nlat = np.squeeze(lats).size
    nlon = np.squeeze(lons).size
    print(f"nlon={nlon} and nlats={nlat}")

    # setup lambert conformal basemap.
    # lat_1 is first standard parallel.
    # lat_2 is second standard parallel (defaults to lat_1).
    # lon_0,lat_0 is central point.
    # rsphere=(6378137.00,6356752.3142) specifies WGS84 ellipsoid
    # area_thresh=1000 means don't plot coastline features less
    # than 1000 km^2 in area.
    m = Basemap(width=nlon * dx, height=nlat * dy,
            rsphere=(6378137.00,6356752.3142),\
            resolution='l',area_thresh=1000.,projection='lcc',\
            lat_1=30.,lat_2=70,lat_0=lat_0,lon_0=lon_0, ax=ax)

    m.drawcoastlines()
    # draw parallels and meridians.
    m.drawparallels(np.arange(-80.,81.,20.))
    m.drawmeridians(np.arange(-180.,181.,20.))
    return m


def plot_field_EuropeNAO_Lambert(lons, lats, field, dx:float=2.5e4, dy:float=2.5e4, 
                                 ax=None, cmap_name="RdBu_r"):
    m = plot_EuropeNAO_Lambert(lons, lats, dx, dy, ax)
    
    X, Y = np.meshgrid(lons, lats) 
    xi, yi = m(X, Y)
    csf = m.contourf(xi, yi, field, extend="both", cmap=cmap_name)
    return m