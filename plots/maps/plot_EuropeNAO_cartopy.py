import numpy as np
import matplotlib.colorbar as mcol
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.mpl.geoaxes import GeoAxes
from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
from mpl_toolkits.axes_grid1 import AxesGrid


def plot_Europe_NAO_Lambert_singlefield(lons, lats, field, cmap_name="RdBu_r",
                                         *args, **kwargs):
    # Compute center of the map
    lon_0 = lons.mean()
    lat_0 = lats.mean()

    # Compute the number of latitures and longitudes 
    nlat = np.squeeze(lats).size
    nlon = np.squeeze(lons).size

    proj = ccrs.LambertConformal(central_longitude=lon_0,
                                 central_latitude=lat_0)

    # Draw a set of axes with coastlines:
    fig = plt.figure(figsize=(6, 4.7))
    #                 [left, bottom, width, height]
    ax = fig.add_axes([0.015, 0.05, 0.975, 0.98], projection=proj)
    ax.coastlines(resolution='110m')
    ax.gridlines()

    im = ax.contourf(lons, lats, field,
                     transform=ccrs.PlateCarree(),
                     cmap=cmap_name, extend='both',
                     **kwargs)
    
    cax,kw = mcol.make_axes(ax,location='bottom',pad=0.05,shrink=0.8)
    
    cbar = plt.colorbar(im, cax=cax, **kw)
    cbar.set_label('500 hPa Geopotential Anomalies [$m^2$ $s^{-2}$]', rotation=0, size=18)
    cbar.ax.tick_params(labelsize=16) 

    return fig, ax


def plot_EUNAO_multiple_shared(lons, lats, nrow:int=1, ncol:int=1,
                                figsize=(18,12), hspace=0.1, wspace=0.02):
    proj = ccrs.LambertConformal(central_longitude=lons.mean(),
                                 central_latitude=lats.mean())
    #
    fig = plt.figure(figsize=figsize)
    widths = np.repeat(10,ncol)
    heights =np.repeat(10,nrow)
    heights = np.append(heights, 1)
    specs = fig.add_gridspec(ncols=ncol, nrows=nrow + 1,
                            width_ratios=widths,
                            height_ratios=heights,
                            wspace=wspace,
                            hspace=hspace) 
    # Create all axes
    ax_dict = {}
    counter = 0
    for i in np.arange(nrow):
        for j in np.arange(ncol):
            counter = counter + 1
            ax = fig.add_subplot(specs[i, j], projection=proj)
            ax.coastlines(resolution='110m')
            ax.gridlines()
            ax.set_aspect("equal")
            ax_dict[counter] = ax

    # Add axis for shared colorbar
    ax_dict["shared"] = fig.add_subplot(specs[nrow, :])
    return fig, ax_dict
                    

def plot_EuropeNAO_Lambert_cartopy(lons, lats, nrow:int=1, ncol:int=1):
    # Compute center of the map
    lon_0 = lons.mean()
    lat_0 = lats.mean()

    # Compute the number of latitures and longitudes 
    nlat = np.squeeze(lats).size
    nlon = np.squeeze(lons).size
    print(f"nlon={nlon} and nlats={nlat}")

    proj = ccrs.LambertConformal(central_longitude=lon_0,
                                 central_latitude=lat_0)

    # If ax is not specified, create a new figure

    fig = plt.figure(figsize=[12, 8])

    ax_dict = {}
    counter = 0
    for i in np.arange(nrow):
        for j in np.arange(ncol):
            counter = counter + 1
            ax_dict[counter] = fig.add_subplot(nrow, ncol, counter, projection=proj)
            

    for key in ax_dict.keys():
        ax = ax_dict[key]
        ax.coastlines(resolution='110m')
        ax.gridlines()
        #ax.set_xticks(np.linspace(lons[0], lons[-1], 5))
        #ax.set_yticks(np.linspace(lats[0], lats[-1], 5))


    """
    fig, axes = plt.subplots(nrows=nrow, ncols=ncol,
                             subplot_kw=dict(projection=proj),
                             figsize=(12, 8))


    if nrow == 1 and ncol ==1:
        axes.coastlines(resolution='110m')
        axes.gridlines()
    else:
        for axe in axes.flatten():
            #ax.set_extent([70,-80,30,30], crs=ccrs.PlateCarree())
            axe.coastlines(resolution='110m')
            axe.gridlines()
    """

    return fig, ax_dict


def plot_field_EuropeNAO_Lambert_cartopy(lons, lats, field, dx:float=2.5e4,
                                         dy:float=2.5e4, do_cb=False,
                                         ax=None, cmap_name="RdBu_r",
                                         *args, **kwargs):
    if ax is None:
        fig, ax = plot_EuropeNAO_Lambert_cartopy(lons, lats, dx, dy, ax)
    
    #X, Y = np.meshgrid(lons, lats) 
    #xi, yi = m(X, Y)

    im = ax.contourf(lons, lats, field,
                     transform=ccrs.PlateCarree(),
                     cmap=cmap_name,  extend='both',
                     **kwargs)
    
    if do_cb:
        cax,kw = mcol.make_axes(ax,location='bottom',pad=0.05,shrink=0.8)
        plt.colorbar(im, cax=cax, **kw)

    return ax, im


def plot_from_cartopy(lons, lats, field):
    projection = ccrs.LambertConformal(central_longitude=lon_0,
                                       central_latitude=lat_0)
    axes_class = (GeoAxes,
                  dict(map_projection=projection))

    fig = plt.figure()
    axgr = AxesGrid(fig, 111, axes_class=axes_class,
                    nrows_ncols=(3, 2),
                    axes_pad=0.6,
                    cbar_location='right',
                    cbar_mode='single',
                    cbar_pad=0.2,
                    cbar_size='3%',
                    label_mode='')  # note the empty label_mode

    for i, ax in enumerate(axgr):
        ax.coastlines()
        ax.set_xticks(np.linspace(-180, 180, 5), crs=projection)
        ax.set_yticks(np.linspace(-90, 90, 5), crs=projection)
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)

        p = ax.contourf(lons, lats, data[i, ...],
                        transform=projection,
                        cmap='RdBu')

    axgr.cbar_axes[0].colorbar(p)

    plt.show()