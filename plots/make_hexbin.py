import matplotlib.pyplot as plt
import numpy as np
import os

def plot_hexbin(x,
                y,
                xlabel=None,
                ylabel=None,
                cmap='jet',
                outsave='.',
                ID="Unknown",
                root_filename="hexbin",
                outfmt="pdf"):
    """plot_df_hexbin
    
    Function that aims to plot an hexagonal binning plot with a unit line
    and with the pearson coeeficient as figure title based on two columns
    of a dataframe.
    
    Parameters
    ----------
    df : [type]
        [description]
    key_x : [type]
        [description]
    key_y : [type]
        [description]
    xlabel : [type], optional
        [description] (the default is None, which [default_description])
    ylabel : [type], optional
        [description] (the default is None, which [default_description])
    cmap : str, optional
        [description] (the default is 'jet', which [default_description])
    outsave : str, optional
        [description] (the default is '.', which [default_description])
    root_filename : str, optional
        [description] (the default is "hexbin", which [default_description])
    outfmt : str, optional
        [description] (the default is "pdf", which [default_description])
    
    Returns
    -------
    [type]
        [description]
    """
    fig, ax = plt.subplots(figsize=(8, 8))
    hb = ax.hexbin(x=x, y=y, cmap=cmap, mincnt=1)
    cbar = fig.colorbar(hb, ax=ax)
    plt.autoscale(enable=True, tight=True)
    #ax.set_ylabel(xlabel, fontsize=24)
    #ax.set_xlabel(ylabel, fontsize=24)
    plt.yticks(fontsize=19)
    plt.xticks(fontsize=19)
    #
    # Change x and y labels if required
    if xlabel:
        plt.xlabel(xlabel, fontsize=21)
    if ylabel:
        plt.ylabel(ylabel, fontsize=21)
    x_unit = min(plt.xlim()[0], plt.ylim()[0])
    y_unit = max(plt.xlim()[1], plt.ylim()[1])
    plt.plot([0, y_unit], [0, y_unit], '--r')
    R2 = np.corrcoef(x,y)[0,1]
    plt.suptitle(f"Station {ID} : $R^2$ = {R2:0.3f}",fontsize=30)
    #
    # get the colorbar
    cbar.ax.set_ylabel('Density', fontsize=24)
    cbar.ax.tick_params(labelsize=19)
    #
    plt.savefig(os.path.join(outsave,
                f"{root_filename}_{ID}.{outfmt}"), format=outfmt,
                bbox_inches='tight')
    return 0
