import matplotlib.pyplot as plt
import numpy as np
import os

def plot_TS(ts,
            ts_labels,
            xlabel=None,
            ylabel=None,
            cmap='jet',
            outsave='.',
            ID="Unknown",
            root_filename="TimeSeries",
            outfmt="pdf"):
    """plot_df_hexbin
    
    Function that aims to plot an hexagonal binning plot with a unit line
    and with the pearson coeeficient as figure title based on two columns
    of a dataframe.
    
    Parameters
    ----------
    df : [type]
        [description]
    key_x : [type]
        [description]
    key_y : [type]
        [description]
    xlabel : [type], optional
        [description] (the default is None, which [default_description])
    ylabel : [type], optional
        [description] (the default is None, which [default_description])
    cmap : str, optional
        [description] (the default is 'jet', which [default_description])
    outsave : str, optional
        [description] (the default is '.', which [default_description])
    root_filename : str, optional
        [description] (the default is "hexbin", which [default_description])
    outfmt : str, optional
        [description] (the default is "pdf", which [default_description])
    
    Returns
    -------
    [type]
        [description]
    """
    fig, ax = plt.subplots(figsize=(12, 8))
    for k, timeserie in enumerate(ts):
        ax.plot(timeserie, label=ts_labels[k])
    plt.yticks(fontsize=19)
    plt.xticks(fontsize=19)
    #
    # Change x and y labels if required
    if xlabel:
        plt.xlabel(xlabel)
    if ylabel:
        plt.ylabel(ylabel)
    plt.suptitle(f"{ID}",fontsize=30)
    #
    plt.savefig(os.path.join(outsave,
                f"{root_filename}_{ID}.{outfmt}"), format=outfmt,
                bbox_inches='tight')
    return 0
