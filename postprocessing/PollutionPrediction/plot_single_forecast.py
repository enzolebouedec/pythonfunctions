import matplotlib.pyplot as plt
import numpy as np
import os

def plot_single_forecast(y_real, y_pred, xlabels, date,
                         outpath=".", fmt="png"):
    fig,ax = plt.subplots()
    ax.plot(range(y_real.size), y_real, label="Measured", marker='+',linestyle='')
    ax.plot(range(y_real.size), y_pred, label="Predicted")
    ax.plot(range(y_real.size), y_real - y_pred, label="Errors", linestyle='--')
    ax.set_xticks(np.arange(0, y_real.size, 5))
    ax.set_xticklabels(['H+' + str(x) for x in np.arange(0, y_real.size, 5)], rotation=45)
    ax.set_ylabel("PM 2.5 [$\mu g. m^{-3}$]")
    ax.set_xlabel("Forecast horizon")
    ax.legend()
    fig.suptitle(f"Forecast {date}")
    fig.savefig(os.path.join(outpath, f"Forecast_{date}.{fmt}"), format=fmt)
    plt.close()
    return 0