import matplotlib.pyplot as plt
import numpy as np
import os


def plot_mae_rmse(err_array, axe=None, outpath=".", fmt="png"):
    plt.close()
    mae = err_array.mean(axe)
    mins = err_array.min(axe)
    maxs = err_array.max(axe)
    time = range(mins.size)
    rmse = np.sqrt(np.square(err_array).mean(axe))
    plt.plot(mae, label="Mean Absolute Error")
    plt.plot(rmse, label="RMSE")
    plt.fill_between(x=time, y1=mins, y2=maxs, alpha=0.4, label="MinsMaxs")
    plt.legend()
    plt.savefig(os.path.join(outpath, f"test.{fmt}"), format=fmt)
    return mae, rmse