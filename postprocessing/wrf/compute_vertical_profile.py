import netCDF4
import wrf

def compute_point_vertical_profiles(ncfile, var, lat, lon, levels, vfield="z", timeidx=0):
    # Get the desired variable
    var = wrf.getvar(ncfile,var)
    # Get the desired vertical field on which to interpolate
    vert_field = wrf.getvar(ncfile, vfield)
    # Make points for vertical cross section
    start = wrf.CoordPair(lat=lat-0.05, lon=lon-0.05)
    end = wrf.CoordPair(lat=lat, lon=lon)
    # Compute The vertical cross section
    vc = wrf.vertcross(field3d=var, vert=vert_field, wrfin=ncfile,timeidx=timeidx,start_point=start, end_point=end, levels=levels)
    return vc