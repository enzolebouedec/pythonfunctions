import numpy as np
import pandas as pd

def mean_difference_matrix(data, absolute=False):
    """Small function to do column-wise mean absolute difference of an array or
    of a pandas dataframe.
    
    Args:
        data (array or DataFrame): data on which to perform the operation
        absolute (bool, optional): Defaults to False. Whether to compute the
        absolute difference or the simple difference. 
    
    Returns:
        array: mean (absolute) difference of Data columwise
    """

    nb_col = data.shape[1]
    matrix = np.zeros((nb_col, nb_col))

    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[0]):

            if type(data) == type(pd.DataFrame()):
                col_i = data[data.keys()[i]]
                col_j = data[data.keys()[j]]
            else:
                col_i = data[:, i]
                col_j = data[:, j]
            
            if absolute:
                matrix[i][j] = np.abs(col_i - col_j).mean()
            else:
                matrix[i][j] = (col_i - col_j).mean()
    return matrix