import numpy as np
import pandas as pd

def bi_sinusoidal_date_encoder(date):
    """bi_sinusoidal_date_encoder Function that aims to make a bi sunosoidal
    representation of a pandas Datetime object
    
    Parameters
    ----------
    date : Pandas DateTime
        [description]
    
    Returns
    -------
    [type]
        [description]
    """
    date_seconds = date.second + date.hour * 3600
    date_day = date.dayofyear
    if date.is_leap_year:
        total_day_in_year = 366
    else:
        total_day_in_year = 365
    sin_day = np.sin(2*np.pi*date_seconds/86400)
    cos_day = np.cos(2*np.pi*date_seconds/86400)
    cos_year = np.cos(2*np.pi*date_day / total_day_in_year)
    sin_year = np.sin(2*np.pi*date_day / total_day_in_year)
    return np.array([cos_year, sin_year, cos_day, sin_day])

def categorical_encoders(date):
    # Encoding day of the week ! Needs to be 6 categorical variables
    # The 7th being not the other 6
    dayofweek= np.zeros(7)
    dayofweek[date.dayofweek] = 1
    dayofweek = dayofweek[:-1]

    # Encoding is weekend !
    if date.dayofweek in [5,6]:
        isweekend = np.array([1])
    else:
        isweekend = np.array([0])

    # Encoding month
    month = np.zeros(12)
    month[date.month - 1] = 1
    month = month[:-1]


    cat_results = np.concatenate((dayofweek, isweekend, month))
    return cat_results

def full_date_encoder(date):
    encoded = np.concatenate((bi_sinusoidal_date_encoder(date),
                              categorical_encoders(date)))
    return encoded

if __name__ == "__main__":
    d1 = pd.to_datetime("17/06/2018 15:00:00")
    d2 = pd.to_datetime("27/12/2018 12:00:00")
    d3 = pd.to_datetime("24/2/2014 04:00:00")
    d4 = pd.to_datetime("16/06/2018 15:00:00")
    dates= [d1, d2, d3, d4]

    print(bi_sinusoidal_date_encoder(d1))
    print(bi_sinusoidal_date_encoder(d2))

    print("=> Categorical variables")
    for date in dates:
        print(f"Processing {date.day_name()} {date}")
        a = categorical_encoders(date)
        print(f"Day of the week encoding: {a[:6]}")
        print(f"Week End encoding: {a[6]}")
        print(f"Month encoding: {a[7:]}")

    print("Full encoder")
    for date in dates:
        print(f"Processing {date.day_name()} {date}")
        print(full_date_encoder(date))

