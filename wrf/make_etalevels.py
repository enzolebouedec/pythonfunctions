# Authored by Enzo Le Bouëdec 28/11/2019
# contact : enzo.lebouedec@univ-grenoble-alpes.fr

import numpy as np
import argparse

def compute_eta(param:float, zlev:int):
    """Function to compute the eta levels according to the tanh fucntion.
    
    Arguments:
        param {float} -- Parameter for the eta level generator function
        zlev {int} -- Number of vertical levels desired
    
    Returns:
        np.array -- Eta levels computed
    """
    eta_levels = np.empty(zlev)
    for k in np.arange(zlev):
        eta_levels[k] = - np.tanh( param * ((k)/(zlev - 1) - 1) ) / np.tanh(param)
    return eta_levels

def eta_as_lines(eta, val_per_row=5):
    """Function to make a generator of eta levels as lines with
    the proper format for the namelist in WRF
    
    Arguments:
        eta {np.array} -- Eta levels desired
    
    Keyword Arguments:
        val_per_row {int} -- Number of values per row for formatting (default: {5})
    
    Yields:
        Generator -- Generator fo lines to be printed in a file
    """
    
    for i in np.arange(start=0,stop=eta.size - eta.size%val_per_row, step=val_per_row):
        line = ""
        for j in range(val_per_row):
            line += f"{eta[i + j]:.8f}, "
        if j == (val_per_row - 1):
            line += "\n"
        yield line
    final_line= ""
    for i in range(eta.size%val_per_row):
        idx = int(np.floor(eta.size/val_per_row) * val_per_row + i)
        final_line += f"{eta[idx]:.8f}, "
    yield final_line

def eta_2_csv(eta, param, zlev, val_per_row):
    with open(f'eta_levels_{param}_{zlev}z.csv', 'w') as f:
        lines = eta_as_lines(eta, val_per_row)
        f.writelines(lines)
    f.close()
    return None

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("eta_param", help="Eta parameter for the function. Typical value 1.14", type=float)
    parser.add_argument("vert_levs", help="Number of vertical levels. Typical value 91", type=int)
    parser.add_argument("--fmt", help="Number of values per row in csv", type=int, default=5)
    args = parser.parse_args()
    # define constants
    print(f"Creating eta levels with value {args.eta_param}")
    eta_levels = compute_eta(param=args.eta_param, zlev=args.vert_levs)
    print(eta_levels)
    eta_2_csv(eta = eta_levels, param=args.eta_param, zlev=args.vert_levs, val_per_row=args.fmt)

