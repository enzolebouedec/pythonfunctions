import numpy as np
from scipy.spatial import cKDTree
from scipy.interpolate import griddata

def interpolate_cross_section(X, Y, Z, cross_start, cross_end, npt=100):
    # Define outputs
    x = np.linspace(cross_start[0], cross_end[0], npt)
    y = np.linspace(cross_start[1], cross_end[1], npt)
    z = np.empty(x.shape)

    # Zip the two matrixes in a list of points
    point_mat = np.array([(i, j) for i, j in zip(X, Y)]).squeeze()
    point_mat = np.swapaxes(point_mat,1,2)
    point_mat = point_mat.reshape((point_mat.shape[0] * point_mat.shape[1], 2))

    # Z flat
    Zfl = Z.reshape(X.shape[0] * X.shape[1])

    # Make the tree
    tree = cKDTree(point_mat)

    for i in range(z.size):
        # Find 3 nearest neigbours
        pt = np.array([x[i],y[i]])
        #print(f"pt={pt}")

        ids = tree.query(pt, k=5)[1]
        #print(f"ids = {ids}")
        #print(f"neighbours = {Zfl[ids]}")

        # Interpolate
        z[i] = griddata(point_mat[ids], Zfl[ids], pt, method='cubic')
        #print(f"z[{i}] = {z[i]}")

    return x,y,z

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    print("Testing if cross section interpolation works")
    print("Regular grid:")
    X= np.linspace(0,2*np.pi)
    Y= np.linspace(0,2*np.pi)
    xx, yy = np.meshgrid(X,Y)
    z = np.sin(xx) + np.cos(yy)

    cross_start = np.array([0,0])
    cross_end = np.array([2*np.pi,2*np.pi])

    a,b,c = interpolate_cross_section(X=xx, Y=yy, Z=z, cross_start=cross_start, cross_end=cross_end)

    sol = np.sin(a) + np.cos(b)
    plt.plot(np.sqrt(a*a + b*b), sol, label="solution")
    plt.plot(np.sqrt(a*a + b*b), c, label="interp")
    plt.plot(np.sqrt(a*a + b*b), 100 *abs(c - sol), label="100 * (abs err)")
    plt.legend()
    plt.show()