import numpy as np

def find_best_config(numfigs: int) -> tuple:
    """Small function to determine the "squarest" arrangement to get numfigs
    figure in a subplots.
    
    Args:
        numfigs (int): Desired number of figure to have in a subplot
    
    Returns:
        tuple: (nrow, ncol) a nice nrow ncol arrangement to display numfigs 
        figure in a subplots
    """

    nrow = np.floor(np.sqrt(numfigs))
    ncol = np.ceil(np.sqrt(numfigs))
    if nrow * ncol < numfigs:
        nrow += 1
    return int(nrow), int(ncol)

if __name__ == "__main__":
    print("------------------------------------------------------------------")
    nfig = 11
    nrow, ncol = find_best_config(nfig)
    print("What is the best plot configuration for %d figures ?" %nfig)
    print("nrow = %d ncol = %d" %(nrow, ncol))

    print("------------------------------------------------------------------")
    nfig = 6
    nrow, ncol = find_best_config(nfig)
    print("What is the best plot configuration for %d figures ?" %nfig)
    print("nrow = %d ncol = %d" %(nrow, ncol))

    print("------------------------------------------------------------------")
    nfig = 23
    nrow, ncol = find_best_config(nfig)
    print("What is the best plot configuration for %d figures ?" %nfig)
    print("nrow = %d ncol = %d" %(nrow, ncol))