import numpy as np
from netCDF4 import Dataset

def compute_slopes(field, dx: float):
    """Function that returns horizontal and vertical slopes
    of field when points in field are spaced by dx
    
    Arguments:
        field {np.array} -- Array of topography / orography 
        dx {float} -- distance between gridpoints in field
    
    Returns:
        [type] -- [description]
    """
    diff_x = np.zeros(field.shape)
    diff_y = np.zeros(field.shape)
    diff_x[0:-1] = field[0:-1] - field[1:]
    diff_y [:,0:-1]= field[:,0:-1] - field[:, 1:]
    horiz_slope = np.arctan( diff_x / dx) * 180/np.pi
    vert_slope = np.arctan(diff_y /dx) * 180/np.pi
    return horiz_slope, vert_slope

if __name__ == "__main__":
    print("Computing slopes for an exemple NetCDF:")

    ds = Dataset("wrfinp")
    hgt = np.array(ds.variables['HGT_M']).squeeze()
    print(hgt)

    hslo, vslo = compute_slopes(hgt, 111.111)


    import matplotlib.pyplot as plt
    plt.pcolormesh(hslo)
    plt.colorbar()
    plt.show()
