from netCDF4 import Dataset
import sys
import numpy as np
import pandas as pd

def load_ncdf_window(path2ncdf, varname, t_ini, t_end):
    ds = Dataset(path2ncdf)
    field_window = ds.variables[varname][t_ini:t_end,:,:]
    return field_window

def load_ncdf_index(path2ncdf, varname, t):
    ds = Dataset(path2ncdf)
    field_window = ds.variables[varname][t]
    ds.close()
    return field_window

def load_ncdf_variable(path2ncdf, varname):
    ds = Dataset(path2ncdf)
    field = ds.variables[varname][:]
    return field

if __name__ == "__main__":
    path2ncdf = '/home/users/lebouede3e/18MOBILAIR/Data/AtmoAURA/27km/original/Data_U500_1117.nc'
    print("return a slice of u")
    print(load_ncdf_window(path2ncdf, 'U_p_500hPa', 35, 55))
