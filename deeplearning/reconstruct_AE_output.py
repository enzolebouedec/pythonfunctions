from keras.models import Model
from keras.layers import Input
import numpy as np

def reconstruct_AE_output(model, generator, layer_name="bottleneck"):
    """Function that aim to cut a Keras model at layer <layer_name> and
    produce outputs out of it.
    
    Arguments:
        model {Keras model} -- A Keras model
        generator {Keras DataGenerator} -- [A Keras DataGenerator]
    
    Keyword Arguments:
        layer_name {str} -- Name of the layer that will be used as input
        for the newly generated model (default: {"bottleneck"})
    
    Returns:
        list -- List containing the model outputs
    """

    # Get the index of the desired layer
    index = None
    for idx, layer in enumerate(model.layers):
        if layer.name == layer_name:
            index = idx
            break

    # Make the reduced model:
    input_shape = model.layers[index].get_input_shape_at(0) # get the input shape of desired layer
    print(f"input shape = {input_shape}")
    layer_input = Input(shape=(22, ))     # a new input tensor to be able to feed the desired layer

    # create the new nodes for each layer in the path
    x = layer_input
    for layer in model.layers[index + 1:]: # The index + 1 comes from the fact that otherwise the results goes twice through the BN
        print(layer.name)
        if not any([x in layer.name for x in ["Q_","T_","R_"]]):
            x = layer(x)

    # create the model
    intermediate_layer_model = Model(layer_input, x)

    # Inp = Input(shape=model.get_layer(layer_name).input_shape)
    # intermediate_layer_model = Model(inputs=Inp,
    #                                  outputs=model.output)
    # print(intermediate_layer_model.summary())
    print(f"generator.shape = {generator.shape}")
    print(intermediate_layer_model.summary())
    # test = np.zeros((1,22))
    to_rec = np.expand_dims(generator,0)
    print(to_rec)
    reduct = intermediate_layer_model.predict(to_rec)
    return reduct