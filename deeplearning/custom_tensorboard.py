from keras import backend as K
from keras.callbacks import TensorBoard

class LRTensorBoard(TensorBoard):
    """
    Custom tensorboard having the following new options:
    -> Track learning rate on epoch end
    """
    def __init__(self, *args, **kwargs):  # add other arguments to __init__ if you need
        super().__init__(*args, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        logs.update({'lr': K.eval(self.model.optimizer.lr)})
        super().on_epoch_end(epoch, logs)