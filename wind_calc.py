import numpy as np


def uv_2_wswd(u: float, v: float):
    """
    Fonction qui calcule la direction d'origine du vent ainsi que son
    intensité en fonction de u et v. u est positif vers l'est et v
    est positif vers le nord. 
    """
    ws = np.sqrt(u*u + v*v)
    wd = (270 - np.arctan2(v, u) * 180/np.pi) % 360
    return ws, wd


def wswd_2_uv(ws: float, wd: float):
    RperD = np.pi / 180
    u = - ws * np.sin(wd * RperD)
    v = - ws * np.cos(wd * RperD)
    return u, v


if __name__ == '__main__':
    print('******************************')
    print('Testing wswd_2_uv')
    print('******************************')

    print('SW wind')
    ws, wd = 2, 225
    print('ws =%f, wd=%f' % (ws, wd))
    u, v = wswd_2_uv(ws, wd)
    print('u = %f | v =%f' % (u, v))

    print('----')
    print('N wind')
    ws, wd = 4, 0
    print('ws =%f, wd=%f' % (ws, wd))
    u, v = wswd_2_uv(ws, wd)
    print('u = %f | v = %f' % (u, v))

    print('----')
    print('NE wind')
    ws, wd = np.sqrt(2), 45
    print('ws =%f, wd=%f' % (ws, wd))
    u, v = wswd_2_uv(ws, wd)
    print('u = %f | v = %f' % (u, v))

    print('******************************')
    print('Testing uv_2_wswd')
    print('******************************')

    print('SW wind')
    u, v = np.sqrt(2), np.sqrt(2)
    print('u =%f, v=%f' % (u, v))
    ws, wd = uv_2_wswd(u, v)
    print('ws = %f | wd = %f' % (ws, wd))

    print('----')
    print('N wind')
    u, v = 0, -4
    print('u =%f, v=%f' % (u, v))
    ws, wd = uv_2_wswd(u, v)
    print('ws = %f | wd = %f' % (ws, wd))

    print('----')
    print('NE wind')
    u, v = -4, -4
    print('u =%f, v=%f' % (u, v))
    ws, wd = uv_2_wswd(u, v)
    print('ws = %f | wd = %f' % (ws, wd))
