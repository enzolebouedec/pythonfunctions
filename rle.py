import numpy as np

def rle(inarray):
    """ run length encoding. Partial credit to R rle function.
        Multi datatype arrays catered for including non Numpy
        returns: tuple (runlengths, startpositions, values) """

    if len(inarray.shape) > 1:
        raise ValueError("Please feed a vector to rle")
    ia = np.asarray( inarray )  # force numpy
    n = len( ia )
    if n == 0:
        return None, None, None
    else:
        y = np.array( ia[1:] != ia[:-1] )  # pairwise unequal (string safe)
        i = np.append( np.where( y ), n - 1 )  # must include last element posi
        z = np.diff( np.append( -1, i ) )  # run lengths
        p = np.cumsum( np.append( 0, z ) )[:-1]  # positions
        return z, p, ia[i]