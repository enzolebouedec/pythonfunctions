import numpy as np
import copy

def movavg_wta(vec, window, how="centered"):
    """movavg_wta
    
    Function that intends to perform a moving average on a vector with discrete
    values. The new values will be equal to the most represented valu in the
    window considered.
    
    Parameters
    ----------
    vec : [type]
        [description]
    window : [type]
        [description]
    
    Returns
    -------
    [type]
        [description]
    """

    if how == "centered":
        modvec = copy.deepcopy(vec)
        # The first values are unchanged while the window does not fit
        half_wdw = int(np.ceil(window / 2))
        id_start = half_wdw
        id_stop = len(vec) - half_wdw + 1
        for idx in range(id_start, id_stop) :
            # print(f"idx = {idx}")
            val, counts = np.unique(vec[idx - half_wdw : idx + half_wdw], return_counts=True)
            # print(f"indexes : {range(idx - half_wdw, idx + half_wdw)}")
            # print(f"vec     : {vec[idx - half_wdw : idx + half_wdw]}")
            # print(f"val = {val} counts = {counts}")
            max_counts = counts[np.argmax(counts)]
            # print(f"max_counts = {max_counts}")
            where_max = np.where(counts == max_counts)[0]
            # print(f"where_max = {where_max}")
            val_max = val[where_max[-1]]
            # print(f"val_max = {val_max}")
            # print(f"{modvec[idx]} -> {val_max}")
            modvec[idx] = val_max
    return modvec


if __name__ == "__main__":
    print("Testing the movavg_wta function")
    print("Test case 1")
    window = 4
    print(f"-----> window = {window}")
    vec = [6, 1, 1, 1, 1, 2, 2, 3, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4]
    modvec = movavg_wta(vec, window = window)
    print(f"original vecotr {vec}")
    print(f"Modified vector {modvec}")

    window = 9
    print(f"-----> window = {window}")
    vec = [6, 1, 1, 1, 1, 2, 2, 3, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4]
    modvec = movavg_wta(vec, window = window)
    print(f"original vecotr {vec}")
    print(f"Modified vector {modvec}")

    print("Test case 2")
    window = 4
    print(f"-----> window = {window}")
    vec = [1, 1, 2, 2, 3, 3, 4, 4]
    modvec = movavg_wta(vec, window = window)
    print(f"original vecotr {vec}")
    print(f"Modified vector {modvec}")
