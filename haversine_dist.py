import math
import numpy as np
from typing import Tuple





def dms_to_dd(d: float, m: float, s: float) -> float:
    """
    From degree minute seconds to decimal degrees
    Parameters
    ----------
    d : float
        degrees
    m : float
        minutes
    s : float
        seconds

    Returns
    -------
    float
        decimal degrees

    """
    return d + m/60 + s/3600


def dmsstr_to_dd(dms: str) -> float:
    """

    Parameters
    ----------
    dms

    Returns
    -------

    """
    if np.all([x in dms for x in ['°', '\'', '\"']]):
        temp = dms.partition('°')
        d = float(temp[0])
        temp = temp[2].partition('\'')
        m = float(temp[0])
        temp = temp[2].partition('\"')
        s = float(temp[0])

        dd = dms_to_dd(d, m, s)
    else:
        raise ValueError("String %s could not be parsed !" % dms)
    return dd


def haversine_distance(p1, p2, verbose=False) -> float:
    """
    Compute the "as a crow flies" distance between to points
    Parameters
    ----------
    p1 : array
        (lat, long) of point 1
    p2 : array
        (lat, long) of point 2
    verbose : bool
        parameter to decide wheter to output infos or not
    Returns
    -------

    """
    # Earths radius
    r = 6371e3  # metres

    # Convert to radians and compute deltas
    lat1 = p2[0] * math.pi / 180
    lat2 = p1[0] * math.pi / 180
    Delta_lat = p2[0] * math.pi / 180 - p1[0] * math.pi / 180
    Delta_long = p2[1] * math.pi / 180 - p1[1] * math.pi / 180

    # Haversine formulas
    a = math.sin(Delta_lat / 2) * math.sin(Delta_lat / 2) \
        + math.cos(lat1) * math.cos(lat2) * math.sin(Delta_long / 2) * math.sin(Delta_long / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    if verbose:
        print('')
        print('------------------------------------')
        print('Haversine distance')
        print('------------------------------------')
        print("Calculating distance between :")
        print("1: %f N %f E" % (p1[0], p1[1]))
        print("2: %f N %f E" % (p2[0], p2[1]))
        print("distance is %f" %(r*c) )
        print('------------------------------------')
        print('')

    return r * c

if __name__ == '__main__':
    # Test dms_to_dd
    dms = [30, 15, 50]
    dd = dms_to_dd(dms[0], dms[1], dms[2])
    print("Calculated decimal degree is %f and should be 30.2638888" %dd)

    point1 = [dms_to_dd(50, 3, 59), dms_to_dd(5, 42, 53)]
    point2 = [dms_to_dd(58, 38, 38), dms_to_dd(3, 4, 12)]

    dist = haversine_distance(point1, point2, verbose=True)

    print("Distance is %f" %dist)
    print("Distance should be 968.9 km")

    print("Test for Nashville and Los Angeles airports")
    Nash = [dms_to_dd(37, 7, 2), - dms_to_dd(86, 40, 2)]
    LosA = [dms_to_dd(33, 56, 4), - dms_to_dd(118, 24, 0)]
    dist_Nash_La = haversine_distance(Nash, LosA, verbose=True)
    print("Distance between Nashville and Los Angeles airport is %f" % dist_Nash_La)
    print("Distance should be 2886.4444 km")