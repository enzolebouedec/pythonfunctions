from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.model_selection import train_test_split
import numpy as np
import sys
path_to_DataProc = "/fsnet/people/lebouede3e/calcul/home/pythonfunctions/"
sys.path.append(path_to_DataProc)
from correlation_matrix import correlation_matrix

def test_classif_robustness(df, clust_alg: str, shuffle=True, **kwargs: str) -> np.ndarray:
    """
    Test of robustness described into Gaillard report on weather type identification over Grenoble area

    Parameters
    ----------
    clust_alg : str
        Name of the clustering algorithm, can be 'Kmeans', 'AgglomerativeClustering'
    n_cluster : int
        Number of clusters for the clustering algortihm, essential parameter
    kwargs :
        Other Parameters for the clustering algorithm

    Returns
    -------
    corr_mat : ndarray
        Correlation matrix between established classifications to check consistency

    """
    # Split State Vector into two vectors
    A,B = train_test_split(df, test_size=0.5, shuffle=shuffle)
    # Perform classification :
    if clust_alg == 'Kmeans':
        kmeansA = KMeans(**kwargs)
        kmeansB = KMeans(**kwargs)
        kmA = kmeansA.fit(A)
        kmB = kmeansB.fit(B)
        labsBA = kmA.predict(B)
        labsB = kmB.labels_
    else:
        raise ValueError('Clustering Algorithm (%s) not recognized ! ' % clust_alg)

    corr_mat = correlation_matrix(labsA=labsB, labsB=labsBA, n_cluster=kmeansA.n_clusters)
    return corr_mat