import os
import sys

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.cluster import DBSCAN, AgglomerativeClustering, KMeans
from sklearn.mixture import GaussianMixture
import joblib


def Perform_clusterings(input_data,
                        KM=True, KM_nclust=8, KM_ninit=100,
                        GMM=True, GMM_ncomp=8, GMM_cov="full", GMM_ninit=10,
                        DBS=True, DB_eps=0.5, DB_minsamp=4,
                        CAH=True, CAH_nclust=8, CAH_link='ward',
                        output_path=".", pkl_name="Classif_results", ID="clust"):

    df = pd.DataFrame()

    if KM:
        print(f"Perform Kmeans")
        km = km = KMeans(n_clusters=KM_nclust, n_init=KM_ninit)
        km.fit(input_data)
        df[f"C_KM_{KM_nclust}c_{KM_ninit}i"] = km.labels_
        # df.to_pickle(os.path.join(output_path, .pkl"))
        joblib.dump(km, os.path.join(output_path,f"{ID}_KM_{KM_nclust}c_{KM_ninit}i"))

    if GMM:
        print(f"Perform GMM")
        gmm = GaussianMixture(n_components=GMM_ncomp, covariance_type=GMM_cov,
                              n_init=GMM_ninit)
        gmm.fit(input_data)
        df[f"C_GMM_{GMM_ncomp}c_{GMM_cov}_{GMM_ninit}i"] = gmm.predict(input_data)
        joblib.dump(gmm, os.path.join(output_path, f"{ID}_GMM_{GMM_ncomp}c_{GMM_cov}_{GMM_ninit}i"))

    if DBS:
        print(f"Perform DBSCAN")
        dbscan = DBSCAN(eps=DB_eps, min_samples=DB_minsamp)
        dbscan.fit(input_data)
        df[f"C_DBS_{DB_minsamp}ms_{DB_eps}eps"] = dbscan.labels_
        joblib.dump(dbscan,os.path.join(output_path, f"{ID}_DBS_{DB_minsamp}ms_{DB_eps}eps"))

    if CAH:
        print(f"Perform CAH")
        cah = AgglomerativeClustering(n_clusters=CAH_nclust, linkage=CAH_link)
        cah.fit(input_data)
        df[f"C_CAH_{CAH_nclust}c_{CAH_link}"] = cah.labels_
        joblib.dump(cah,os.path.join(output_path, f"{ID}_CAH_{CAH_nclust}c_{CAH_link}"))

    df.set_index(input_data.index, inplace=True)
    pd.to_pickle(df, os.path.join(output_path, pkl_name))
    
    return df