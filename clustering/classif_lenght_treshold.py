import numpy as np
import pandas as pd
from pythonfunctions.rle import rle

def classif_length_tresh(classif, tresh, cluster_num=99):
    lengths, position, value = rle(classif.values.squeeze())
    tmp = np.zeros(classif.values.size)
    for i, length in enumerate(lengths):
        if length >= tresh:
            #print(f"tmp[{position[i]}: {position[i] + length}] = {value[i]}")
            tmp[position[i]: position[i] + length] = value[i]
        else:
            tmp[position[i]: position[i] + length] = cluster_num
    mod_classif = pd.DataFrame(tmp.astype('int'), index = classif.index, columns = classif.columns)
    return mod_classif