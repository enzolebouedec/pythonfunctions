import sys
path = "/fsnet/people/lebouede3e/calcul/home/pythonfunctions/"
sys.path.append(path)
from rle import rle
from best_plot_config import find_best_config
import matplotlib.pyplot as plt
import numpy as np
import os


def plot_episode_length_histograms(classification, output_path=".", save_id="",
                                   save_format="pdf"):
    """Function that makes a plot of lengths distribution of the episodes
    present in classif_ts. It uses a rle method to do so.
    
    Args:
        classif_ts (np.array): Array of integers describing the classification
            of a time serie
    
    Returns:
        tuple: Matplotlib objects describing a plots
    """

    lengths, pos, vals = rle(classification)
    nrow, ncol = find_best_config(np.unique(vals).shape[0])
    fig, ax = plt.subplots(nrow, ncol, figsize=(16, 10))
    for k, val in enumerate(np.unique(vals)):
        id_val = vals.squeeze() == val
        mean = lengths[id_val].mean()
        med = np.quantile(lengths[id_val],0.5)
        ratio = lengths[id_val].sum() / lengths.sum()
        ax.flatten()[k].hist(lengths[id_val], bins=np.arange(0, 500, 12))
        ax.flatten()[k].set_title(f"Cluster {val}: {(ratio * 100):.2f}%")
        ax.flatten()[k].axvline(mean, color='r', linestyle='--', label=f"Mean = {mean:.2f}")
        ax.flatten()[k].axvline(mean, color='r', linestyle='-', label=f"Median = {med:.2f}")
        ax.flatten()[k].legend()

    fig.savefig(os.path.join(output_path, f"{save_id}_episode_length_distrib.{save_format}"),
                format=save_format)
    return fig, ax