import pandas as pd

def Date_2_1117_Index(date):
    t0 = pd.to_datetime('2011-01-01 00:00:00')
    delta_hour = pd.to_timedelta(date - t0).total_seconds()/3600
    return delta_hour

if __name__ == "__main__":
    from netCDF4 import Dataset
    print("Loading ERA_SLP_1117 as ds")
    ds = Dataset('/home/users/lebouede3e/18MOBILAIR/Data/ERA5/era5_for_pm25_prediction/ERA5_slp_1117.nc')

    # Date in the begining
    t1 = pd.to_datetime('2012-01-01 02:00:00')
    print(f"Processing date {t1}")
    
    idx=Date_2_1117_Index(t1) 
    time_in_ds = ds.variables['time'][idx]
    print(f"ds['time'][{idx}]= {time_in_ds}")

    print(f"Recompute the date from ERA date definition :")
    print(pd.to_datetime('1900-01-01 00:00:00') + pd.Timedelta(hours=float(time_in_ds)))

    # Date in the end
    t1 = pd.to_datetime('2017-11-21 02:00:00')
    print(f"Processing date {t1}")
    
    idx=Date_2_1117_Index(t1) 
    time_in_ds = ds.variables['time'][idx]
    print(f"ds['time'][{idx}]= {time_in_ds}")

    print(f"Recompute the date from ERA date definition :")
    print(pd.to_datetime('1900-01-01 00:00:00') + pd.Timedelta(hours=float(time_in_ds)))